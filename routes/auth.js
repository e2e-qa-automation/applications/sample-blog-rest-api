const authControllers = require('../controllers/auth')

const setupAuthRoutes = (app) => {
  app.post('/auth/login', authControllers.login)
  app.post('/auth/register', authControllers.register)
}

module.exports = setupAuthRoutes
