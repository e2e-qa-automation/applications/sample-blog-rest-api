const setupAuthRoutes = require('./auth')
const setupCommentsRoutes = require('./comments')
const setupDocsRoutes = require('./docs')
const setupPostsRoutes = require('./posts')
const setupRatingsRoutes = require('./ratings')
const setupUsersRoutes = require('./users')

const setupRoutes = (app) => {
  setupDocsRoutes(app)

  setupAuthRoutes(app)
  setupCommentsRoutes(app)
  setupPostsRoutes(app)
  setupRatingsRoutes(app)
  setupUsersRoutes(app)
}

module.exports = setupRoutes
