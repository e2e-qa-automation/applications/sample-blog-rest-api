const usersControllers = require('../controllers/users')

const setupUsersRoutes = (app) => {
  app.post('/users', usersControllers.createNewUser)
  app.get('/users', usersControllers.findUsers)
  app.get('/users/:userId', usersControllers.findUser)
}

module.exports = setupUsersRoutes
