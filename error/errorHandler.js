const ApplicationError = require('./ApplicationError')

const errorHandler = (e, res) => {
  console.log('error', e)
  if (e instanceof ApplicationError) {
    res.status(e.statusCode).json({ code: e.errorCode, details: e.errorDetails })
  } else if (e instanceof s.StructError) {
    res.status(400).json(e.failures().map((f) => ({ key: f.key, message: f.message })))
  } else if (e instanceof PrismaClientValidationError) {
    res.status(400).json(e.message)
  } else if (e.message) {
    res.status(500).json(e.message)
  } else {
    res.status(500).json(JSON.stringify(e))
  }
}

module.exports = errorHandler
