const superstruct = require('superstruct')

const prisma = require('../prisma/client')

const schemasForPosts = {
  createPayload: superstruct.object({
    writer_id: superstruct.number(),
    content: superstruct.string(),
  }),
  updatePayload: superstruct.optional({
    content: superstruct.string(),
  }),
}

const postsServices = {
  createNewPost: (createPayload) => {
    superstruct.assert(payload, schemasForPosts.createPayload)
    return prisma.post.create({
      data: createPayload,
    })
  },
  findPost: (postId) => {
    return prisma.post.findUnique({ where: { post_id: postId } })
  },
  findPosts: () => {
    return prisma.post.findMany({})
  },
  updatePost: (postId, updatePayload) => {
    superstruct.assert(updatePayload, schemasForPosts.updatePayload)
    return prisma.post.update({
      where: {
        post_id: postId,
      },
      data: updatePayload,
    })
  },
}

module.exports = postsServices
