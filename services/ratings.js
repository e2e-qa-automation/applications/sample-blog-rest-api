const superstruct = require('superstruct')

const prisma = require('../prisma/client')

const schemasForRatings = {
  createPayload: superstruct.object({
    post_id: superstruct.number(),
    user_id: superstruct.number(),
    value: superstruct.number(),
  }),
}

const ratingsServices = {
  createNewRating: (createPayload) => {
    superstruct.assert(payload, schemasForRatings.createPayload)
    return prisma.rating.create({
      data: createPayload,
    })
  },
  findRating: (ratingId) => {
    return prisma.rating.findUnique({ where: { rating_id: ratingId } })
  },
  findRatings: () => {
    return prisma.rating.findMany({})
  },
}

module.exports = ratingsServices
