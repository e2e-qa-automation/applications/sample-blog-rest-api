const superstruct = require('superstruct')

const prisma = require('../prisma/client')
const authServices = require('./auth')

const schemasForUsers = {
  createPayload: superstruct.object({
    username: superstruct.string(),
    password: superstruct.string(),
  }),
  updatePayload: superstruct.optional({
    content: superstruct.string(),
  }),
}

const usersServices = {
  createNewUser: (createPayload) => {
    superstruct.assert(createPayload, schemasForUsers.createPayload)
    return prisma.user.create({
      data: {
        username: createPayload.username,
        hash: authServices.passwords.hashPassword(createPayload.password),
      },
    })
  },
  findUser: (userId) => {
    return prisma.user.findUnique({ where: { user_id: userId } })
  },
  findUserByUsername: (userName) => {
    return prisma.user.findUnique({ where: { username: userName } })
  },
  findUsers: (requestQuery) => {
    console.log('Find users service', 'with request queries', requestQuery)
    if (requestQuery.username) return prisma.user.findUnique({ where: { username: requestQuery.username } })
    return prisma.user.findMany({})
  },
  updateUser: (userId, updatePayload) => {
    superstruct.assert(updatePayload, schemasForUsers.updatePayload)
    return prisma.user.update({
      where: {
        user_id: userId,
      },
      data: updatePayload,
    })
  },
}

module.exports = usersServices
