const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')

const environment = require('../environment')

const authServices = {
  passwords: {
    comparePasswords: (dbPassword, requestPassword) => {
      return bcrypt.compareSync(requestPassword, dbPassword)
    },
    hashPassword: (requestPassword) => {
      const salt = bcrypt.genSaltSync(environment.bcryptSalt)
      return bcrypt.hashSync(requestPassword, salt)
    },
  },
  tokens: {
    getTokenFromUser: (user) => {
      return jsonwebtoken.sign(user, environment.jwtSecret)
    },
    getTokenPayload: (token) => {
      return jsonwebtoken.decode(token, { complete: true, json: true })
    },
    verifyTokenValidity: (token) => {
      return jsonwebtoken.verify(token, environment.jwtSecret)
    },
  },
}

module.exports = authServices
