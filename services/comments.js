const superstruct = require('superstruct')

const prisma = require('../prisma/client')

const schemasForComments = {
  createPayload: superstruct.object({
    post_id: superstruct.number(),
    user_id: superstruct.number(),
    content: superstruct.string(),
  }),
  updatePayload: superstruct.optional({
    content: superstruct.string(),
  }),
}

const commentsServices = {
  createNewComment: (createPayload) => {
    superstruct.assert(payload, schemasForComments.createPayload)
    return prisma.comment.create({
      data: createPayload,
    })
  },
  findComment: (commentId) => {
    return prisma.comment.findUnique({ where: { comment_id: commentId } })
  },
  findComments: () => {
    return prisma.comment.findMany({})
  },
  updateComment: (commentId, updatePayload) => {
    superstruct.assert(updatePayload, schemasForComments.updatePayload)
    return prisma.comment.update({
      where: {
        comment_id: commentId,
      },
      data: updatePayload,
    })
  },
}

module.exports = commentsServices
