const cors = require('cors')
const express = require('express')

const logReqInfo = require('./logReqInfo')

const setupMiddlewares = (app) => {
  app.use(logReqInfo)
  app.use(express.json())
  app.use(cors())
}

module.exports = setupMiddlewares
