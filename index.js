const environment = require('./environment')
const server = require('./server')

server.listen(environment.httpPort, () => {
  console.log('API is up !', 'Listening on', environment.httpPort)
})
