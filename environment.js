const dotenv = require('dotenv')

dotenv.config()

const environment = {
  bcryptSalt: parseInt(process.env.BCRYPT_SALT) || 8,
  httpPort: process.env.PORT || 5001,
  jwtSecret: process.env.JWT_SECRET || 'secret',
}

module.exports = environment
