const { expect } = require('chai')

const usersServices = require('../../services/users')
const chaiHttpPromise = require('../../test-utils/chaiHttpPromise')
const randomUtils = require('../../test-utils/random-utils')

// @use-case/user-registration @api
describe('User case: User registration', function() {
  let jordanUsername
  let jordanPassword
  let kevinUsername
  let kevinPassword
  beforeEach(async function() {
    jordanUsername = 'jordanounet' + randomUtils.generateRandomDigits(6),
    jordanPassword = '@#$jordanounet' + randomUtils.generateRandomDigits(2) + randomUtils.generateRandomUpperCases(2)
    kevinUsername = 'kevinounet' + randomUtils.generateRandomDigits(6),
    kevinPassword = '@#$kevinounet' + randomUtils.generateRandomDigits(2) + randomUtils.generateRandomUpperCases(2)
    await usersServices.createNewUser({
      username: kevinUsername,
      password: kevinPassword,
    })
  })

  // @persona/jordan @nominal
  describe('Scenario: On REST API, Users could register for new account on /auth/register route', function() {
    let chaiHttpResponse

    it('When Jordan registers for a new account by API', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/register',
        {
          username: jordanUsername,
          password: jordanPassword,
        },
        null,
      )
    })

    it('Then Jordan receives a CREATED response', function() {
      expect(chaiHttpResponse.status).to.eq(201)
    })

    it('And Jordan receives in response his User object\'s payload', function() {
      expect(chaiHttpResponse.body.user).to.be.an('object')
    })

    it('And Jordan receives in response his authentication token', function() {
      expect(chaiHttpResponse.body.token).to.be.a('string')
    })
  })

  // @persona/jordan @rule/username-required @bad-request
  describe('Scenario: On REST API, Users could not register for new account without username', function() {
    let chaiHttpResponse

    it('When Jordan registers for a new account by API and ommit username', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/register',
        {
          password: jordanPassword,
        },
        null,
      )
    })

    it('Then Jordan receives a BAD REQUEST response', function() {
      expect(chaiHttpResponse.status).to.eq(400)
    })

    it('And Jordan receives in response a code "rule/username-required"', function() {
      expect(chaiHttpResponse.body.code).to.eq('rule/username-required')
    })

    it('And Jordan\'s user account is not created', async function() {
      const jordan = await usersServices.findUserByUsername(jordanUsername)
      expect(jordan).to.eq(null)
    })
  })

  // @persona/jordan @rule/username-unique @conflict
  describe('Scenario: On REST API, Users cannot register for new account with existing username', function() {
    let chaiHttpResponse

    it('When Jordan registers for a new account by API and provides same username as Kevin', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/register',
        {
          username: kevinUsername,
          password: jordanPassword,
        },
        null,
      )
    })

    it('Then Jordan receives a CONFLICT response', function() {
      expect(chaiHttpResponse.status).to.eq(409)
    })

    it('And Jordan receives in response a code "rule/username-unique"', function() {
      expect(chaiHttpResponse.body.code).to.eq('rule/username-unique')
    })

    it('And Jordan\'s user account is not created', async function() {
      const jordan = await usersServices.findUserByUsername(jordanUsername)
      expect(jordan).to.eq(null)
    })
  })

  // @persona/jordan @rule/password-required @bad-request
  describe('Scenario: On REST API, Users cannot register for new account without a password', function() {
    let chaiHttpResponse

    it('When Jordan registers for a new account by API and ommit password', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/register',
        {
          username: jordanUsername,
        },
        null,
      )
    })

    it('Then Jordan receives a BAD REQUEST response', function() {
      expect(chaiHttpResponse.status).to.eq(400)
    })

    it('And Jordan receives in response a code "rule/password-required"', function() {
      expect(chaiHttpResponse.body.code).to.eq('rule/password-required')
    })

    it('And Jordan\'s user account is not created', async function() {
      const jordan = await usersServices.findUserByUsername(jordanUsername)
      expect(jordan).to.eq(null)
    })
  })

  // @persona/jordan @rule/password-length @rule/password-upper-cases @rule/password-digits @rule/password-special-chars @bad-request
  const tests = [
    ['rule/password-length', 'pass'],
    ['rule/password-upper-cases', 'passswwwwwoooorrrrddd'],
    ['rule/password-digits', 'PppAaaSssWwwOooRrrDdd'],
    ['rule/password-special-chars', 'Ppp1Aaa3Sss4Www5Ooo6Rrr7Ddd8'],
  ]

  tests.forEach(([rule, passwordCandidate]) => {
    describe(`Scenario: On REST API, Users cannot register for new account with an invalid password (${rule})`, function() {
      let chaiHttpResponse

      it(`When Jordan registers for a new account by API and provides an invalid password (${rule})`, async function() {
        chaiHttpResponse = await chaiHttpPromise(
          'post',
          '/auth/register',
          {
            username: jordanUsername,
            password: passwordCandidate,
          },
          null,
        )
      })

      it('Then Jordan receives a BAD REQUEST response', function() {
        expect(chaiHttpResponse.status).to.eq(400)
      })

      it(`And Jordan receives in response a code "${rule}"`, function() {
        expect(chaiHttpResponse.body.code).to.eq(rule)
      })

      it('And Jordan\'s user account is not created', async function() {
        const jordan = await usersServices.findUserByUsername(jordanUsername)
        expect(jordan).to.eq(null)
      })
    })
  })
})
