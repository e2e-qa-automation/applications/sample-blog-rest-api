const { expect } = require('chai')

const usersServices = require('../../services/users')
const chaiHttpPromise = require('../../test-utils/chaiHttpPromise')
const randomUtils = require('../../test-utils/random-utils')

// @use-case/user-authentication @api
describe('Use case: User authentication', function() {
  let kevinUsername
  let kevinPassword
  beforeEach(async function() {
    kevinUsername = 'kevinounet' + randomUtils.generateRandomDigits(6),
    kevinPassword = '@#$kevinounet' + randomUtils.generateRandomDigits(2) + randomUtils.generateRandomUpperCases(2)
    await usersServices.createNewUser({
      username: kevinUsername,
      password: kevinPassword,
    })
  })

  // @persona/kevin @nominal
  describe('Scenario: On REST API, Users could authenticate on /auth/login route', function() {
    let chaiHttpResponse

    it('When Kevin authenticates himself by API', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/login',
        {
          username: kevinUsername,
          password: kevinPassword,
        },
        null,
      )
    })

    it('Then Kevin receives an OK response', function() {
      expect(chaiHttpResponse.status).to.eq(200)
    })

    it('And Kevin receives in response his User object\'s payload', function() {
      expect(chaiHttpResponse.body.user).to.be.an('object')
    })

    it('And Kevin receives in response his authentication token', function() {
      expect(chaiHttpResponse.body.token).to.be.a('string')
    })
  })

  // @persona/kevin @rule/username-required @bad-request
  describe('Scenario: On REST API, Users cannot authenticate without a username', function() {
    let chaiHttpResponse

    it('When Kevin authenticates himself by API and ommit username', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/login',
        {
          password: kevinPassword,
        },
        null,
      )
    })

    it('Then Kevin receives a BAD REQUEST response', function() {
      expect(chaiHttpResponse.status).to.eq(400)
    })

    it('And Kevin receives in response a code "rule/username-required"', function() {
      expect(chaiHttpResponse.body.code).to.eq('rule/username-required')
    })

    it('And Kevin do not receive in response his authentication token', function() {
      expect(chaiHttpResponse.body.token).to.be.undefined
    })
  })

  // @persona/kevin @rule/user-registered @not-found
  describe('Scenario: On REST API, Users cannot authenticate with unregistered username', function() {
    let chaiHttpResponse

    it('When Kevin authenticates himself by API and provides unregistered username', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/login',
        {
          username: 'qwertyuiopasdfghjklzxcvbnm',
          password: kevinPassword,
        },
        null,
      )
    })

    it('Then Kevin receives a NOT FOUND response', function() {
      expect(chaiHttpResponse.status).to.eq(404)
    })

    it('And Kevin receives in response a code "rule/user-registered"', function() {
      expect(chaiHttpResponse.body.code).to.eq('rule/user-registered')
    })

    it('And Kevin do not receive in response his authentication token', function() {
      expect(chaiHttpResponse.body.token).to.be.undefined
    })
  })

  // @persona/kevin @rule/password-required @bad-request
  describe('Scenario: On REST API, Users cannot authenticate without password', function() {
    let chaiHttpResponse

    it('When Kevin authenticates himself by API and ommit password', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/login',
        {
          username: kevinUsername,
        },
        null,
      )
    })

    it('Then Kevin receives a BAD REQUEST response', function() {
      expect(chaiHttpResponse.status).to.eq(400)
    })

    it('And Kevin receives in response a code "rule/password-required"', function() {
      expect(chaiHttpResponse.body.code).to.eq('rule/password-required')
    })

    it('And Kevin do not receive in response his authentication token', function() {
      expect(chaiHttpResponse.body.token).to.be.undefined
    })
  })

  // @persona/kevin @rule/password-match @bad-request
  describe('Scenario: On REST API, Users cannot authenticate with bad password', function() {
    let chaiHttpResponse

    it('When Kevin authenticates himself by API and provides bad password', async function() {
      chaiHttpResponse = await chaiHttpPromise(
        'post',
        '/auth/login',
        {
          username: kevinUsername,
          password: 'password',
        },
        null,
      )
    })

    it('Then Kevin receives a BAD REQUEST response', function() {
      expect(chaiHttpResponse.status).to.eq(400)
    })

    it('And Kevin receives in response a code "rule/password-match"', function() {
      expect(chaiHttpResponse.body.code).to.eq('rule/password-match')
    })

    it('And Kevin do not receive in response his authentication token', function() {
      expect(chaiHttpResponse.body.token).to.be.undefined
    })
  })
})
