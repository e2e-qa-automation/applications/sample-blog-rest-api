const superstruct = require('superstruct')

const ApplicationError = require('../error/ApplicationError')
const errorHandler = require('../error/errorHandler')
const prisma = require('../prisma/client')
const authServices = require('../services/auth')
const usersServices = require('../services/users')

const schemas = {
  common: superstruct.object({
    username: superstruct.string(),
    password: superstruct.string(),
  }),
}

const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/

const authControllers = {
  login: async (req, res, next) => {
    try {
      if (!req.body.username) throw new ApplicationError(400, 'rule/username-required', 'username is required for authentication')
      if (!req.body.password) throw new ApplicationError(400, 'rule/password-required', 'password is required for authentication')
      superstruct.assert(req.body, schemas.common)
      const targetUser = await usersServices.findUserByUsername(req.body.username)
      if (!targetUser) throw new ApplicationError(404, 'rule/user-registered', 'user not found by username')
      const isPasswordValid = authServices.passwords.comparePasswords(targetUser.hash, req.body.password)
      if (!isPasswordValid) throw new ApplicationError(400, 'rule/password-match', 'user password mismatch')
      const authToken = authServices.tokens.getTokenFromUser(targetUser)
      res.status(200).json({ user: targetUser, token: authToken })
    } catch (e) {
      errorHandler(e, res)
    } finally {
    }
    return next()
  },
  register: async (req, res, next) => {
    try {
      if (!req.body.username) throw new ApplicationError(400, 'rule/username-required', 'username is required for registration')
      if (!req.body.password) throw new ApplicationError(400, 'rule/password-required', 'password is required for registration')
      superstruct.assert(req.body, schemas.common)
      if (req.body.password.length < 8) throw new ApplicationError(400, 'rule/password-length', 'password length must be 8 or more')
      if (!/[A-Z]/.test(req.body.password)) throw new ApplicationError(400, 'rule/password-upper-cases', 'password must include at least one upper case character')
      if (!/[0-9]/.test(req.body.password)) throw new ApplicationError(400, 'rule/password-digits', 'password must include at least one digit character')
      if (!specialChars.test(req.body.password)) throw new ApplicationError(400, 'rule/password-special-chars', 'password must include at least one special character')
      const targetUser = await usersServices.findUserByUsername(req.body.username)
      if (targetUser) throw new ApplicationError(409, 'rule/username-unique', 'username is already registered')
      const hash = authServices.passwords.hashPassword(req.body.password)
      const newUser = await prisma.user.create({
        data: {
          username: req.body.username,
          hash,
        },
      })
      const authToken = authServices.tokens.getTokenFromUser(newUser)
      res.status(201).json({ user: newUser, token: authToken })
    } catch (e) {
      errorHandler(e, res)
    } finally {
    }
    return next()
  },
}

module.exports = authControllers
