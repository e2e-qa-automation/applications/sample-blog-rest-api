const postsServices = require('../services/posts')

// const schemas = {}

const postsControllers = {
  createNewPost: async (req, res, next) => {
    try {
      const newPost = await postsServices.createNewPost(req.body)
      res.status(201).json(newPost)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
  findPost: async (req, res, next) => {
    try {
      const post = await postsServices.findPost(req.params.postId)
      res.status(200).json(post)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
  findPosts: async (req, res, next) => {
    try {
      const posts = await postsServices.findPosts()
      res.status(200).json(posts)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
}

module.exports = postsControllers
