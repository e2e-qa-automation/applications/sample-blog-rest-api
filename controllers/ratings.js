const ratingsServices = require('../services/ratings')

// const schemas = {}

const ratingsControllers = {
  createNewRating: async (req, res, next) => {
    try {
      const newRating = await ratingsServices.createNewRating(req.body)
      res.status(201).json(newRating)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
  findRating: async (req, res, next) => {
    try {
      const rating = await ratingsServices.findRating(req.params.ratingId)
      res.status(200).json(rating)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
  findRatings: async (req, res, next) => {
    try {
      const ratings = await ratingsServices.findRatings()
      res.status(200).json(ratings)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
}

module.exports = ratingsControllers
