const commentsServices = require('../services/comments')

// const schemas = {}

const commentsControllers = {
  createNewComment: async (req, res, next) => {
    try {
      const newComment = await commentsServices.createNewComment(req.body)
      res.status(201).json(newComment)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
  findComment: async (req, res, next) => {
    try {
      const comment = await commentsServices.findComment(req.params.commentId)
      res.status(200).json(comment)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
  findComments: async (req, res, next) => {
    try {
      const comments = await commentsServices.findComments()
      res.status(200).json(comments)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
}

module.exports = commentsControllers
