const ApplicationError = require('../error/ApplicationError')
const errorHandler = require('../error/errorHandler')
const usersServices = require('../services/users')

// const schemas = {}

const usersControllers = {
  createNewUser: async (req, res, next) => {
    try {
      const newUser = await usersServices.createNewUser(req.body)
      res.status(201).json(newUser)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
  findUser: async (req, res, next) => {
    try {
      const user = await usersServices.findUser(req.params.userId)
      res.status(200).json(user)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
  findUsers: async (req, res, next) => {
    try {
      const users = await usersServices.findUsers(req.query)
      if (!users) throw new ApplicationError(404, 'not_found', 'No User found with given criteria(s)')
      res.status(200).json(users)
    } catch (e) {
      errorHandler(e, res)
    } finally {
      return next()
    }
  },
}

module.exports = usersControllers
