const chai = require('chai')
const chaiHttp = require('chai-http')

const app = require('../app')

chai.use(chaiHttp)

/**
 *
 * @param {'get' | 'post' | 'patch' | 'delete'} method
 * @param {String} url
 * @param {Object} payload?
 * @param {Object} headers?
 * @return {Promise<Response>}
 */
const chaiHttpPromise = (method, url, payload, headers) => {
  return new Promise((resolve, reject) => {
    if (method === 'get') {
      chai.request(app)
        .get(url)
        .end((err, res) => {
          if (err) reject(err)
          else resolve(res)
        })
    } else if (method === 'post') {
      chai.request(app)
        .post(url)
        .send(payload)
        .end((err, res) => {
          if (err) reject(err)
          else resolve(res)
        })
    }
  })
}

module.exports = chaiHttpPromise
