const apiResponse = require('./api-response')

const customAssert = {
  apiResponse,
}

module.exports = customAssert
