const isBadRequest = require('./isBadRequest')
const isConflict = require('./isConflict')
const isCreated = require('./isCreated')
const isNotFound = require('./isNotFound')
const isOk = require('./isOk')

const apiResponse = {
  isBadRequest,
  isConflict,
  isCreated,
  isNotFound,
  isOk,
}

module.exports = apiResponse
