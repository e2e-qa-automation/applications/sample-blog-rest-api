const assert = require('assert')

const isNotFound = (apiResponse) => {
  assert.equal(apiResponse.status, 404)
}

module.exports = isNotFound
