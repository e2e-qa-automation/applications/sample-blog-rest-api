const { expect } = require('chai')

const isCreated = (apiResponse) => {
  expect(apiResponse.status).to.eq(201)
}

module.exports = isCreated
