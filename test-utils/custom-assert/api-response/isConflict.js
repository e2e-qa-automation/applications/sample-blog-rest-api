const { expect } = require('chai')

const isConflict = (apiResponse) => {
  expect(apiResponse.status).to.eq(409)
}

module.exports = isConflict
