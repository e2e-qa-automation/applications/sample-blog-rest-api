const assert = require('assert')

const isOk = (apiResponse) => {
  assert.equal(apiResponse.status, 200)
}

module.exports = isOk
