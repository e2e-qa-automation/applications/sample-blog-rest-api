const { expect } = require('chai')

const isBadRequest = (apiResponse) => {
  expect(apiResponse.status).to.eq(400)
}

module.exports = isBadRequest
