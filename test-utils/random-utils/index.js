const generateRandomDigits = require('./generateRandomDigits')
const generateRandomLowerCases = require('./generateRandomLowerCases')
const generateRandomUpperCases = require('./generateRandomUpperCases')
const generateRandomUserBase = require('./generateRandomUserBase')

const randomUtils = {
  generateRandomDigits,
  generateRandomUpperCases,
  generateRandomLowerCases,
  generateRandomUserBase,
}

module.exports = randomUtils
