const generateRandomDigits = require('./generateRandomDigits')
const generateRandomLowerCases = require('./generateRandomLowerCases')
const generateRandomUpperCases = require('./generateRandomUpperCases')

const generateRandomUserBase = () => ({
  username: `user_${generateRandomLowerCases(3)}_${generateRandomDigits(3)}`,
  password: `P@ss_${generateRandomUpperCases(5)}_${generateRandomLowerCases(5)}_${generateRandomDigits(5)}`,
})

module.exports = generateRandomUserBase
