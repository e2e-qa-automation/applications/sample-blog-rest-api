const baseCases = 'abcdefghijklmnopqrstuvwxyz'

const getRandomLowerCase = () => baseCases.charAt(Math.floor(Math.random() * baseCases.length))

const generateRandomLowerCases = (numberOfDigits) => {
  const digits = []
  for (let i = 0; i < numberOfDigits; i += 1) {
    const digit = getRandomLowerCase()
    digits.push(digit)
  }
  return digits.join('')
}

module.exports = generateRandomLowerCases
