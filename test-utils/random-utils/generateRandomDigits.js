const baseDigits = '0123456789'

const getRandomDigit = () => baseDigits.charAt(Math.floor(Math.random() * baseDigits.length))

const generateRandomDigits = (numberOfDigits) => {
  const digits = []
  for (let i = 0; i < numberOfDigits; i += 1) {
    const digit = getRandomDigit()
    digits.push(digit)
  }
  return digits.join('')
}

module.exports = generateRandomDigits
