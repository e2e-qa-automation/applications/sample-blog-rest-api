const baseCases = 'ABCDEFGHIKLMNOPQRSTUVWXYZ'

const getRandomUpperCase = () => baseCases.charAt(Math.floor(Math.random() * baseCases.length))

const generateRandomUpperCases = (numberOfDigits) => {
  const digits = []
  for (let i = 0; i < numberOfDigits; i += 1) {
    const digit = getRandomUpperCase()
    digits.push(digit)
  }
  return digits.join('')
}

module.exports = generateRandomUpperCases
